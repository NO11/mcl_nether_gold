local modname = minetest.get_current_modname()
local S = minetest.get_translator(modname)

minetest.register_node("mcl_nether_gold:nether_gold_ore", {
	description = S("Nether Gold Ore"),
	_doc_items_longdesc = S("Nether gold ore is an ore containing gold nuggets. It is commonly found around netherrack in the Nether."),
	stack_max = 64,
	tiles = { "mcl_nether_netherrack.png^mcl_nether_gold_ore.png" },
	is_ground_content = true,
	groups = { pickaxey = 1, building_block = 1, material_stone = 1, xp = 0 },
	drop = {
		max_items = 1,
		items = {
			{ items = { "mcl_core:gold_nugget 6" }, rarity = 5 },
			{ items = { "mcl_core:gold_nugget 5" }, rarity = 5 },
			{ items = { "mcl_core:gold_nugget 4" }, rarity = 5 },
			{ items = { "mcl_core:gold_nugget 3" }, rarity = 5 },
			{ items = { "mcl_core:gold_nugget 2" } },
		}
	},
	sounds = mcl_sounds.node_sound_stone_defaults(),
	_mcl_blast_resistance = 3,
	_mcl_hardness = 3,
	_mcl_silk_touch_drop = true,
	_mcl_fortune_drop = mcl_core.fortune_drop_ore
})

minetest.register_craft({
	type = "cooking",
	output = "mcl_core:gold_ingot",
	recipe = "mcl_nether_gold:nether_gold_ore",
	cooktime = 10,
})

if minetest.settings:get_bool("mcl_generate_ores", true) then
	for _, g in pairs({ { 850, 4, 3 }, { 1650, 8, 4 } }) do
		minetest.register_ore({
			ore_type = "scatter",
			ore = "mcl_nether_gold:nether_gold_ore",
			wherein = { "mcl_nether:netherrack", "mcl_core:stone" },
			clust_scarcity = g[1],
			clust_num_ores = g[2],
			clust_size = g[3],
			y_min = mcl_vars.mg_nether_min,
			y_max = mcl_vars.mg_nether_max,
		})
	end
end